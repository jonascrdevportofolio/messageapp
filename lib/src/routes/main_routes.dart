import 'package:flutter/widgets.dart';
import 'package:messages/src/pages/account/login_page.dart';
import 'package:messages/src/pages/account/register_page.dart';
import 'package:messages/src/pages/messages/chats_page.dart';
import 'package:messages/src/pages/messages/message_page.dart';

Map<String, WidgetBuilder> getMainRoutes(){
  return {
    LoginPage().namePage : (BuildContext context) => LoginPage(),
    RegisterPage().namePage : (BuildContext context) => RegisterPage(),
    ChatsPage().namePage : (BuildContext context) => ChatsPage(),
    MessagesPage().namePage : (BuildContext context) => MessagesPage()
  };
}