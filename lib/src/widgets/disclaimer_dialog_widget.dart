import 'package:flutter/material.dart';

class DisclaimerDialogWidget extends StatefulWidget {

  @override
  _DisclaimerDialogWidgetState createState() => _DisclaimerDialogWidgetState();
}

class _DisclaimerDialogWidgetState extends State<DisclaimerDialogWidget> {
  bool doNotShowAgain = false;

  @override
  Widget build(BuildContext context) {

    return AlertDialog(
      actions: <Widget>[
        FlatButton(
          child: Text('OK'),
          onPressed: () => Navigator.pop(context, doNotShowAgain),
        )
      ],
      title: Text('Warning'),
      content: Container(
        height: 140,
        child: Column(
          children: <Widget>[
            Text('This app is a demostration purpose only. It is fully functional but with no security rules. Don\'t share sensitive content here.'),
            CheckboxListTile(
              onChanged: (value) {
                setState((){
                  doNotShowAgain = value;
                });
              },
              title: Text('OK, I get it'),
              value: doNotShowAgain,
            )
          ],
        ),
      )
    );
  }
}