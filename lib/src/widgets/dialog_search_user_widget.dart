import 'package:flutter/material.dart';
import 'package:messages/src/models/user_model.dart';
import 'package:messages/src/providers/account_providers.dart';
import 'package:messages/src/providers/loading_providers.dart';

class SearchUserDialogWidget extends StatefulWidget {
  final UserModel currentUser;

  SearchUserDialogWidget({Key key, @required this.currentUser}) : super(key: key);


  static final formKey = GlobalKey<FormState>();

  @override
  _SearchUserDialogWidgetState createState() => _SearchUserDialogWidgetState();
}

class _SearchUserDialogWidgetState extends State<SearchUserDialogWidget> {
  String _email = '';

  String _msnError = '';

  final _loadingProvider = LoadingProvider();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      actions: <Widget>[
        FlatButton(
          textColor: Colors.grey,
          child: Text('Cancel'),
          onPressed: () => Navigator.pop(context),
        ),
        FlatButton(
          textColor: Theme.of(context).primaryColor,
          child: Text('Search'),
          onPressed: () => _searchUser(context),
        )
      ],
      content: Form(
        onChanged: () => _msnError = '',
        key: SearchUserDialogWidget.formKey,
        child: TextFormField(
          initialValue: 'jonascr.dev@gmail.com',
          keyboardType: TextInputType.emailAddress,
          decoration: InputDecoration(
            labelText: 'Email\'s user',
            errorText: _msnError.length > 0 ? _msnError : null
          ),
          onSaved: (value) => _email = value,
          validator: (value){
            if (value == widget.currentUser.email) return 'This is your own email';
            return null;
          },
        )
      ),
    );
  }

  _searchUser(BuildContext context) async {
    if (!SearchUserDialogWidget.formKey.currentState.validate()) return;

    SearchUserDialogWidget.formKey.currentState.save();

    _loadingProvider.start(context);
    final getUser = await accountProviders.getUserFromEmail(_email);
    _loadingProvider.stop(context);
    
    if (!getUser.ok){
      setState(() {
        _msnError = getUser.msn;
      });
      return;
    }
    

    Navigator.pop(context,getUser.result);
  }
}