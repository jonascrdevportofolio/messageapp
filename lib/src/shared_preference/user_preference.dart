import 'package:shared_preferences/shared_preferences.dart';




class UserPreference {
  static final UserPreference _instance = new UserPreference._();

  factory UserPreference(){
    return _instance; 
  }

  final String _disclamer = 'showDisclamer';


  UserPreference._();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();    
  }

  clearAll(){
    _prefs.clear();
  }

  ///Query History
  get dontShowDisclaimer {
    return _prefs.getBool(_disclamer) ?? false;
  }

  set dontShowDisclaimer(bool value) {
    _prefs.setBool(_disclamer, value);
  }
}