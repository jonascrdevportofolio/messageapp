import 'package:cloud_firestore/cloud_firestore.dart';

class Messages {
  List<MessageModel> items = [];

  Messages(List<DocumentSnapshot> list){
    list.forEach((doc){
      final message = MessageModel.fromFirebase(doc);
      items.add(message);
    });
  }
}

class MessageModel {
  String displayName;
  String idMessage;
  String idUserEntry;
  DateTime dateEntry;
  String message;

  MessageModel({
    this.displayName,
    this.idMessage,
    this.message,
    this.dateEntry,
    this.idUserEntry
  });

  MessageModel.fromFirebase(DocumentSnapshot data){
    this.idMessage = data.documentID;
    this.message = data.data["message"];
    this.idUserEntry = data.data["idUserEntry"];
    this.dateEntry = data.data["dateEntry"];
    this.displayName = data.data["displayName"];
  }

  toFirebase() => {
    'display': this.displayName,
    'message': this.message,
    'idUserEntry': this.idUserEntry,
    'dateEntry': this.dateEntry,
    'displayName' : this.displayName
  };
}