


import 'package:cloud_firestore/cloud_firestore.dart';

class UserModel {




  String idUser;
  String displayName;
  String email;
  DateTime lastSeen;
  String photoURL;
  
  UserModel({
    this.displayName,
    this.email,
    this.lastSeen,
    this.photoURL,
    this.idUser
  });

  UserModel.fromFirebase(DocumentSnapshot data){
    this.idUser = data.documentID;
    this.email = data.data["email"];
    this.photoURL = data.data["photoURL"];
    this.displayName = data.data["displayName"];
    this.lastSeen = data.data["lastSeen"];
  }



  toFirebase(UserModel user) {
    Map<String,dynamic> response = {};
    if (this.email != null) response.putIfAbsent("email", () => this.email);
    if (this.displayName != null) response.putIfAbsent("displayName", () => this.displayName);
    if (this.lastSeen != null) response.putIfAbsent("lastSeen", () => this.lastSeen);
    if (this.photoURL != null) response.putIfAbsent("photoURL", () => this.photoURL);
    return response;
  }

  setLastSeen(){
    this.lastSeen = DateTime.now();
  }
}