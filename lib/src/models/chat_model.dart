import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:messages/src/models/message_model.dart';
import 'package:messages/src/models/provider_response_model.dart';
import 'package:messages/src/models/user_model.dart';
import 'package:messages/src/providers/messages_providers.dart';
import 'package:messages/src/utils/constante.dart';

class Chats {
  List<ChatModel> items = [];

  Chats(List<DocumentSnapshot> list){
    list.forEach((doc){
      final conversation = ChatModel.fromFirebase(doc);
      items.add(conversation);
    });
  }
}

class ChatModel {
  String idChat;
  List<String> members;
  DateTime dateLastMessage;
  String lastMessage;
  Map<String, int> unread; //{'idUser1': 2, 'idUser2': 0}
  List<MessageModel> messages;
  Map<String, dynamic> name; //{'idUser1': userName2, 'idUser2': userName1}
  Map<String, dynamic> urlImage; //{'idUser1': userPhoto2, 'idUser2': userPhoto1}

  ChatModel({
    this.dateLastMessage,
    this.idChat,
    this.lastMessage,
    this.members,
    this.unread,
    this.messages,
    this.urlImage,
    this.name
  }){
    if (this.members == null){
      this. members = List();
    }
  }

  ChatModel.fromFirebase(DocumentSnapshot snap){
    this.idChat = snap.documentID;
    this.members = snap.data["members"].cast<String>().toList();
    this.dateLastMessage = snap.data["dateLastMessage"];
    this.lastMessage = snap.data["lastMessage"];
    this.unread = Map<String, int>.from(snap.data["unread"]);
    this.urlImage = Map<String, String>.from(snap.data["urlImage"]);
    this.name = Map<String, String>.from(snap.data["name"]);
  }

  ChatModel.create(List<UserModel> users){
    this.members = [];
    this.unread = {};
    this.name = {};
    this.urlImage = {};
    users.forEach((user){
      
      this.members.add(user.idUser);
      this.unread.putIfAbsent(user.idUser, () => 0);
      this.name.putIfAbsent(user.idUser, (){
        if (users.length == 2){
          final i = users.indexOf(user);
          final otherUser = i == 0 ? 1 : 0;
          return users[otherUser].displayName;
        }
        String response = '';
        users.forEach((u) => response = response + u.displayName + ' ');
        return response;
      });
      this.urlImage.putIfAbsent(user.idUser, () {
        if (users.length == 2){
          final i = users.indexOf(user);
          final otherUser = i == 0 ? 1 : 0;
          return users[otherUser].photoURL;
        }
        return userDefaultImage;
      });
    });
  }

  Future<ProviderResponse<MessageModel>> addMessage(MessageModel msn) async {
    this.lastMessage = msn.message;
    this.dateLastMessage = msn.dateEntry;
    this.unread[_getOtherUser(msn.idUserEntry)]++;
    return await messageProvider.addMessage(this.idChat, msn);
  }

  Map<String, dynamic> toFirebase() => {
    'members': this.members,
    'dateLastMessage': this.dateLastMessage,
    'lastMessage': this.lastMessage,
    'unread': this.unread,
    'unread': this.unread,
    'urlImage': this.urlImage,
    'name': this.name,
  };

  String _getOtherUser(String idUser){
    String response;
    this.members.forEach((u){
      if (u != idUser) response = u;
    });

    return response;
  }

  

  
}