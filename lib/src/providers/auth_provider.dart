import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:messages/src/models/provider_response_model.dart';
import 'package:messages/src/models/user_model.dart';
import 'package:rxdart/rxdart.dart';


class AuthProvider {

  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final Firestore _db = Firestore.instance;

  //The user from firebase auth
  Observable<FirebaseUser> user;
  //The user from profile
  Observable<UserModel> profile;

  PublishSubject<bool> loading = PublishSubject<bool>();

  AuthProvider(){
    user = Observable(_auth.onAuthStateChanged);

    profile = user.switchMap((FirebaseUser u) {
      if (u != null) {
        return _db.collection('users').document(u.uid).snapshots().map((snap) => UserModel.fromFirebase(snap));
      }else {
        return Observable.just(null);
      }
    });
  }


  Future<ProviderResponse<UserModel>> emailSignIn(String email, String password) async {
    loading.add(true);
    ProviderResponse response = ProviderResponse<UserModel>();
    try {
      FirebaseUser userFb = await _auth.signInWithEmailAndPassword(email: email, password: password);
      UserModel user = UserModel(
        idUser: userFb.uid,
        lastSeen: DateTime.now(), 
        displayName: userFb.displayName, 
        email: userFb.email,
        photoURL: userFb.photoUrl
      );
      updateUserData(user);
      print('sign in : ' + user.displayName);
      response.ok = true;
      response.msn = 'Signed in!';
      response.result = user;
    } catch (e) {
      print(e);
      response.ok = false;
      response.msn = e.message;
    }
    

    loading.add(false);
    return response;
  }

  Future<ProviderResponse<UserModel>> googleSignIn() async  {
    loading.add(true);
    ProviderResponse response = ProviderResponse<UserModel>();

    try {
      //Sign in in google but not in firebase
      GoogleSignInAccount googleUser = await _googleSignIn.signIn();

      //Get the google auth
      GoogleSignInAuthentication googleAuth = await googleUser.authentication;
      FirebaseUser userFb = await _auth.signInWithGoogle(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken
      );

      UserModel user = UserModel(
        idUser: userFb.uid,
        lastSeen: DateTime.now(), 
        displayName: userFb.displayName, 
        email: userFb.email,
        photoURL: userFb.photoUrl
      );

      updateUserData(user);
      print('sign in : ' + userFb.displayName);
      response.ok = true;
      response.result = user;
      
    } catch (e) {
      print(e);
      response.ok = false;
      response.msn = e.message;
    }

    
    loading.add(false);

    return response;
  }

  Future<ProviderResponse<FirebaseUser>> createAccount(UserModel user, password) async {

    ProviderResponse response = ProviderResponse<FirebaseUser>();
    try {
      FirebaseUser firebaseUser = await _auth.createUserWithEmailAndPassword(
        email: user.email,
        password: password
      );

      user.setLastSeen();
      user.idUser = firebaseUser.uid;

      UserUpdateInfo info = new UserUpdateInfo();
      info.displayName = user.displayName;
      firebaseUser.updateProfile(info);
      updateUserData(user);

      response.ok = true;
      response.result = firebaseUser;
    }catch (e) {
      print(e);
      response.ok = false;
      response.msn = e.message;
    }
    

    return response;
  }

  void updateUserData(UserModel user) async {

    DocumentReference ref = _db.collection('users').document(user.idUser);

    final data = user.toFirebase(user);

    return ref.setData(data, merge: true);

  }

  Stream<UserModel> getProfile(String idUser){
    return this._db.collection('users').document(idUser).snapshots().map((snap) => UserModel.fromFirebase(snap));
  }

  void signOut(){
    _auth.signOut();
  }



}


final AuthProvider authProvider = AuthProvider();