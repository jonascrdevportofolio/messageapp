
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:messages/src/models/provider_response_model.dart';
import 'package:messages/src/models/user_model.dart';

class AccountProviders {
    final CollectionReference _db = Firestore.instance.collection('users');

    AccountProviders();

    Future<UserModel> getUserFromID(String idUser)async{
      final promise = await _db.document(idUser).get();
      return UserModel.fromFirebase(promise);
    }

    Future<ProviderResponse<UserModel>> getUserFromEmail(String email)async{
      final promise = await _db.where('email', isEqualTo: email).getDocuments();

      ProviderResponse response = ProviderResponse<UserModel>();
      if (promise.documents.isNotEmpty){
        response.result = UserModel.fromFirebase(promise.documents.first);
        response.ok = true;
      }else {
        response.ok = false;
        response.msn = 'No user with this email';
      }
      
      return response;
    }
}


final accountProviders = AccountProviders();