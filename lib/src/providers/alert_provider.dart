import 'package:flutter/material.dart';
import 'package:flushbar/flushbar.dart';

class AlertProvider {

  // final BuildContext context;
  
  
  AlertProvider();

  void showSuccessMessage(String msn, BuildContext context){
    Flushbar(
      flushbarPosition: FlushbarPosition.TOP,
      duration: Duration(milliseconds: 2000),
      message: msn ?? '',
      icon: Icon(Icons.check_circle_outline, color: Colors.green),
      margin: EdgeInsets.all(8),
      borderRadius: 8,
    )..show(context);
  }

  void showErrorMessage(String msn, BuildContext context){
    Flushbar(
      flushbarPosition: FlushbarPosition.TOP,
      duration: Duration(milliseconds: 2000),
      message: msn ?? '',
      icon: Icon(Icons.error_outline, color: Colors.red),
      margin: EdgeInsets.all(8),
      borderRadius: 8,
    )..show(context);
  }
}

final alertProvider = AlertProvider();