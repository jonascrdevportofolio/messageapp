import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:messages/src/models/chat_model.dart';
import 'package:messages/src/models/message_model.dart';
import 'package:messages/src/models/provider_response_model.dart';


class ChatProvider{
  final Firestore _db = Firestore.instance;

  ChatProvider();

  Stream<List<MessageModel>> getConversation(String idConversation){
    return _db.collection('conversations/$idConversation/message').snapshots().map((snap) => Messages(snap.documents).items);
  }

  Stream<List<ChatModel>> getConversationList(String idUser){
    return _db.collection('chats').where('members', arrayContains: idUser).snapshots().map((snap) => Chats(snap.documents).items);
  }

  Future<ProviderResponse<ChatModel>> createNewConversation(ChatModel chat) async{
    ProviderResponse response = ProviderResponse<ChatModel>();
    try {
      final newChat = await this._db.collection('chats').add(chat.toFirebase());
      chat.idChat = newChat.documentID;
      response.result = chat;
      response.ok = true;
      response.msn = 'Chat created !';

    } catch (e) {
      response.ok = false;
      response.msn = e.message;
    }

    return response;
  }

  Future<ProviderResponse<bool>> updateChat(ChatModel chat) async {
    ProviderResponse<bool> response = ProviderResponse<bool>();
    try {
      await this._db.collection('chats').document(chat.idChat).setData(chat.toFirebase(), merge:true);
      response.result = true;
      response.ok = true;
      response.msn = 'Chat updated !';

    } catch (e) {
      response.result = false;
      response.ok = false;
      response.msn = e.message;
    }

    return response;
  }
}


final chatProvider = ChatProvider();