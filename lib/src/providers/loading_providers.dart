import 'package:flutter/material.dart';
import 'package:messages/src/widgets/loading_widget.dart';
class LoadingProvider {
  String msn;

  LoadingProvider({
    this.msn = 'Espere, por favor'
  });

  start(BuildContext context){
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          
          elevation: 0,
          backgroundColor: Colors.transparent,
          content: _createBlockedLoading()
        );
      }
    );
  }


  stop(BuildContext context){
    Navigator.pop(context);
  }


  Widget _createBlockedLoading(){
    return AlertDialog(
      elevation: 0,
      backgroundColor: Colors.transparent,
      content: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            LoadingWidget(),
            SizedBox(height: 20),
            Chip( label:Text(msn))
          ],
        ),
      )
    );
  }
}