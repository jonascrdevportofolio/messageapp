import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:messages/src/models/message_model.dart';
import 'package:messages/src/models/provider_response_model.dart';

class MessagesProviders {
  final _db = Firestore.instance;

  Stream<List<MessageModel>> getChatMessage(String idChat){
    return _db.collection('chats/$idChat/messages').orderBy('dateEntry', descending: true).snapshots().map((snap) => Messages(snap.documents).items);
  }

  Future<ProviderResponse<MessageModel>> addMessage(String idChat, MessageModel message) async  {
    ProviderResponse<MessageModel> response = ProviderResponse();
    try {
      final add = await _db.collection('chats/$idChat/messages').add(message.toFirebase());
      message.idMessage = add.documentID;
      response.ok = true;
      response.result = message;
      response.msn = 'OK';
    } catch (e) {
      response.ok = false;
      response.msn = e.message;
    }

    return response;
  }
}

final messageProvider = MessagesProviders();