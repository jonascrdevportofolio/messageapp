import 'package:flutter/material.dart';
import 'package:messages/src/models/chat_model.dart';
import 'package:messages/src/models/user_model.dart';
import 'package:messages/src/pages/messages/message_page.dart';
import 'package:messages/src/providers/alert_provider.dart';
import 'package:messages/src/providers/auth_provider.dart';
import 'package:messages/src/providers/chat_provider.dart';
import 'package:messages/src/shared_preference/user_preference.dart';
import 'package:messages/src/utils/constante.dart';
import 'package:messages/src/widgets/dialog_search_user_widget.dart';
import 'package:messages/src/widgets/disclaimer_dialog_widget.dart';
import 'package:messages/src/widgets/loading_widget.dart';


class ChatsPage extends StatefulWidget {
  ChatsPage({Key key,this.idCurrentUser}) : super(key: key);

  final String namePage = 'conversations';
  final String idCurrentUser;

  _ChatsPageState createState() => _ChatsPageState();
}

class _ChatsPageState extends State<ChatsPage> {

  UserModel _user;
  AlertProvider _alertProvider = AlertProvider();
  final userPref = UserPreference();
  @override
  void initState() {
    super.initState();
    if (!userPref.dontShowDisclaimer){
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        final hide = await showDialog<bool>(
          context: context,
          builder: (BuildContext context) => DisclaimerDialogWidget()
        );

        userPref.dontShowDisclaimer = hide;
      });
    }
    authProvider.getProfile(widget.idCurrentUser).listen((user){
      if (user != null){
        setState(() {
          _user = user;
        });
      }
    });
  }
  

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Conversations'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: (){},
          ),
          IconButton(
            icon: Icon(Icons.clear),
            onPressed: authProvider.signOut,
          )
        ],
      ),
      body: _createBody(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.person_add),
        onPressed: () => _createNewChat(context),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      // floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
    );
  }

  Widget _createBody() {
    if (_user == null) return LoadingWidget();
    return StreamBuilder(
      stream: chatProvider.getConversationList(_user.idUser),
      builder: (BuildContext context, AsyncSnapshot<List<ChatModel>> snapshot){
        if (snapshot.hasData){
          return _createConversationList(snapshot.data);
        }
        return LoadingWidget();
      },
    );
  }

  Widget _createConversationList(List<ChatModel> data) {
    return ListView.builder(
      itemCount: data.length,
      itemBuilder: (BuildContext context, int i) {
        final conv = data[i];
        return ListTile(
          leading: CircleAvatar(
            backgroundImage: NetworkImage(conv.urlImage[_user.idUser]?? userDefaultImage),
          ),
          title: Text(conv.name[_user.idUser]),
          subtitle: Text(conv.lastMessage?? ''),
          trailing: conv.unread[_user.idUser] > 0
                    ?  Chip(
                        label: Text(conv.unread[_user.idUser].toString()),
                        backgroundColor: Colors.greenAccent[100],
                      )
                    : Icon(Icons.chevron_right),
          onTap: () {
            final argument = {
              'user' : _user,
              'chat': conv
            };
            Navigator.pushNamed(context, MessagesPage().namePage, arguments: argument);
          },
        );
     },
    );
  }

  _createNewChat(BuildContext context) async{
    UserModel user = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return SearchUserDialogWidget(currentUser: _user,);
      }
    );

    final newChat = ChatModel.create([user, _user]);

    final createChat = await chatProvider.createNewConversation(newChat);
    if (createChat.ok){
      _alertProvider.showSuccessMessage(createChat.msn, context);
    }else {
      _alertProvider.showErrorMessage(createChat.msn, context);
    }

  }
}