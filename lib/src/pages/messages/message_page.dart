import 'package:flutter/material.dart';
import 'package:messages/src/models/chat_model.dart';
import 'package:messages/src/models/message_model.dart';
import 'package:messages/src/models/user_model.dart';
import 'package:messages/src/providers/alert_provider.dart';
import 'package:messages/src/providers/chat_provider.dart';
import 'package:messages/src/providers/messages_providers.dart';
import 'package:messages/src/widgets/loading_widget.dart';

class MessagesPage extends StatefulWidget {
  MessagesPage({Key key}) : super(key: key);

  final String namePage = 'chatRoom';

  _MessagesPageState createState() => _MessagesPageState();
}

class _MessagesPageState extends State<MessagesPage> {
  UserModel _currentUser;
  ChatModel _chat;
  final textController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final Map<String, dynamic> argument = ModalRoute.of(context).settings.arguments;

    _currentUser = argument['user'];
    _chat = argument['chat'];

    final _size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text(_chat.name[_currentUser.idUser]),
        centerTitle: true,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Container(
              padding: EdgeInsets.all(10),
              child: _createMessageList(_size)
            ),
          ),
          Container(
              color: Colors.white,
              padding: EdgeInsets.all(10.0),
              width: double.infinity,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      controller: textController,
                        decoration: new InputDecoration(
                          hintText: 'Chat message',
                        ),
                    ),
                  ),
                  Container(
                    width: 50,
                    height: 40,
                    alignment: Alignment.center,
                    child: FlatButton(
                      textColor: Colors.white,
                      color: Theme.of(context).primaryColor,
                      child: Icon(Icons.send),
                      onPressed: _addMessage
                    ),
                  )
                ],
              ),
          ),
        ],
      ),
    );
  }

  Widget _createMessageList(Size size){
    if (_chat == null) return Container();
    return StreamBuilder(
      stream: messageProvider.getChatMessage(_chat.idChat) ,
      builder: (BuildContext context, AsyncSnapshot<List<MessageModel>> snapshot){
        if (!snapshot.hasData) return LoadingWidget();
        final list = snapshot.data;
        return ListView.builder(
          reverse: true,
          itemCount: list.length,
          itemBuilder: (BuildContext context, int i) {
            final msn = list[i];
            _chat.unread[_currentUser.idUser] = 0;
            chatProvider.updateChat(_chat);
            return _createMessage(msn, size);
         },
        );
      },
    );
  }


  Widget _createMessage(MessageModel msn, Size size){
    if (msn.idUserEntry == _currentUser.idUser){
      return Container(
        child: Row(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  width: size.width * 0.7,
                  child: Text('${msn.displayName} - ${msn.dateEntry.day}.${msn.dateEntry.month}.${msn.dateEntry.year}',
                    textAlign: TextAlign.left,

                    style: TextStyle(
                      fontSize: 12,
                      fontStyle: FontStyle.italic
                    ),
                  ),
                ),
                SizedBox(height: 7),
                Container(
                  width: size.width * 0.7,
                  padding: EdgeInsets.all(10),
                  child: Text(msn.message),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.blue[50],

                  ),
                )
              ]
            ),
            Expanded(child: Container()),
          ],
        ),
      );
    }
    return Container(
      margin: EdgeInsets.only(bottom: 20),
      child: Row(
        children: <Widget>[
          Expanded(child: Container()),
          Column(
            children: <Widget>[
              Container(
                width: size.width * 0.7,
                child: Text('${msn.displayName} - ${msn.dateEntry.day}.${msn.dateEntry.month}.${msn.dateEntry.year}',
                  textAlign: TextAlign.right,

                  style: TextStyle(
                    fontSize: 12,
                    fontStyle: FontStyle.italic
                  ),
                ),
              ),
              SizedBox(height: 7),
              Container(
                width: size.width * 0.7,
                padding: EdgeInsets.all(10),
                child: Text(msn.message, textAlign: TextAlign.right,),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.green[100],
                ),
              )
            ]
          ),
        ],
      ),
    );
  }

  _addMessage() async {
    final msn = MessageModel(
      dateEntry: DateTime.now(),
      displayName: _currentUser.displayName,
      message: textController.text,
      idUserEntry: _currentUser.idUser
    );

    final add = await _chat.addMessage(msn);
    if (!add.ok){
      alertProvider.showErrorMessage(add.msn, context);
    }

    textController.text = '';
  }
}