import 'package:flutter/material.dart';
import 'package:messages/src/pages/account/register_page.dart';
import 'package:messages/src/providers/alert_provider.dart';
import 'package:messages/src/providers/auth_provider.dart';
import 'package:messages/src/providers/loading_providers.dart';

const double LOGIN_PAGE = 0.0;
const double REGISTER_PAGE = 1.0;

class LoginPage extends StatefulWidget {
  const LoginPage({Key key}) : super(key: key);

  final String namePage = 'login';

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  final loading = LoadingProvider();
  final alertProvider = AlertProvider();

  final _heroKey = UniqueKey();

  String _email = '';
  String _password = '';
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            color: Color.fromRGBO(79, 189, 172, 1),
          ),
          _createBackgroundCircle(size),
          _createContent(context, size),
        ],
      )
    );
  }

  Widget _createBackgroundCircle(Size size) {
    return Positioned(
      width: size.height,
      top: - size.height * 0.3,
      left: - size.height *0.25,
      child: Container(
        height: size.height +100,
        decoration: BoxDecoration(
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.black26,
              blurRadius: 3.0,
              offset: Offset(0.0,5.0),
              spreadRadius: 3
            )
          ],
          color: Colors.white,
          borderRadius: BorderRadius.circular(1000)
        ),
      ),
    );
  }

  Widget _createContent(BuildContext context, Size size) {
    
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SizedBox(height: 65),
          Text('LOGIN', style: TextStyle(fontSize: 25, fontWeight: FontWeight.w900) ),
          SizedBox(height: 50),
          _createFormBox(size),
          _createActionButtons(context, size),
        ],
      ),
    );
  }

  Widget _createFormBox(Size size){
    return Stack(
      overflow: Overflow.visible,
      children: [
        _createLoginForm(size),
        _createIconAccount(size),
        _createLoginButton(size)
      ],
    );
  }

  Widget _createLoginForm(Size size) {
    return Container(
      padding: EdgeInsets.all(30),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(7),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black26,
            blurRadius: 20.0,
            spreadRadius: 10
          )
        ]
      ),
      width: size.width * 0.7,
      child: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            SizedBox(height: 30,),
            _createUserNameField(),
            SizedBox(height: 30,),
            _createPasswordField(),
            SizedBox(height: 50,),
          ],
        ),
      )
    );
  }

  Widget _createIconAccount(Size size) {
    return Positioned(
      top: -35,
      left: (size.width * 0.7) / 2 - 35,
      child: Container(
        height: 70,
        width: 70,
        decoration: BoxDecoration(
          color: Color.fromRGBO(79, 189, 172, 1),
          borderRadius: BorderRadius.circular(70),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              spreadRadius: 5
            )
          ]
        ),
        child: SizedBox(
          width: double.infinity,
          child: Icon(Icons.person, size: 50, color: Colors.white)
        ),
      )
    );
  }

  Widget _createLoginButton(Size size) {
    return Positioned(
      bottom: -25,
      left: (size.width * 0.7) / 2 - 80,
      child: RaisedButton(
        elevation: 15,
        color: Color.fromRGBO(79, 189, 172, 1),
        textColor: Colors.white,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 40, vertical: 15),
          child: Text('Login', style: TextStyle(fontSize: 20),)
        ),
        onPressed: _onSubmit,
      ),
    );
  }

  Widget _createActionButtons(BuildContext context, Size size){
    return Container(
      margin: EdgeInsets.only(top: 30),
      width: size.width * 0.7,
      child: Column(
        children: <Widget>[
          FlatButton(
            child: Text('Forgot password'),
            onPressed: (){},
          ),
          FlatButton(
            child: Text('Create Account'),
            onPressed: () => Navigator.pushNamed(context, RegisterPage().namePage, arguments: _heroKey),
          ),
          _createGoogleLoginField(),
        ],
      ),
    );
  }

  void _onSubmit() async {
    if (!_formKey.currentState.validate()) return;
    _formKey.currentState.save();
    loading.start(context);
    print(_email);
    print(_password);
    final login = await authProvider.emailSignIn(_email, _password);
    loading.stop(context);
    if (!login.ok){
      alertProvider.showErrorMessage(login.msn, context);
    }
  }

  Widget _createUserNameField() {
    return Container(
      child: TextFormField(
        initialValue: _email,
        keyboardType: TextInputType.emailAddress,
        textAlign: TextAlign.center,
        decoration: _inputDecoration('Email'),
        onSaved: (value) => _email = value,
      )
    );
  }

  Widget _createPasswordField(){
    return Container(
      child: TextFormField(
        initialValue: _password,
        textAlign: TextAlign.center,
        obscureText: true,
        decoration: _inputDecoration('Password'),
        validator: (value){
          if (value.length < 6) return 'Password too short';
          return null;
        },
        onSaved: (value) => _password = value,
      )
    );
  }
  Widget _createGoogleLoginField(){
    return RaisedButton(
      elevation: 10,
      child: Row(
        children: <Widget>[
          Image(
            height: 20,
            image: AssetImage('assets/images/google-icon.png'),
          ),
          Expanded(child: Text(
            'Sign in with Google',
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.grey),
          ))
        ],
      ),
      color: Colors.white,
      onPressed: ()async {
        loading.start(context);
        final googleSignIn = await authProvider.googleSignIn();
        loading.stop(context);
        if (!googleSignIn.ok){
          alertProvider.showErrorMessage(googleSignIn.msn, context);
        }
      },
    );
  }

  _inputDecoration(String label) {
    return InputDecoration(
      labelText: label,
      filled: true,
      fillColor: Color.fromRGBO(79, 189, 172, 0.1),
      hasFloatingPlaceholder: false,
      enabledBorder: OutlineInputBorder(
        borderSide: new BorderSide(color: Colors.transparent),
        borderRadius: const BorderRadius.all(
          Radius.circular(10.0),
        )),
      border: OutlineInputBorder(
        borderSide: new BorderSide(color: Colors.transparent),
        borderRadius: const BorderRadius.all(
          Radius.circular(10.0),
        ),
        
      ),
      
    );
  }

}