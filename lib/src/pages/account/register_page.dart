import 'package:flutter/material.dart';
import 'package:messages/src/models/user_model.dart';
import 'package:messages/src/providers/alert_provider.dart';
import 'package:messages/src/providers/auth_provider.dart';
import 'package:messages/src/providers/loading_providers.dart';
import 'package:messages/src/validators/user_validator.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key key}) : super(key: key);
  final String namePage = 'register';

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {

  UserModel _user = new UserModel();
  final _keyFrom = GlobalKey<FormState>();
  String password, confirmPassword;
  final _passKey = GlobalKey<FormFieldState>();
  final _passConfirmKey = GlobalKey<FormFieldState>();
  LoadingProvider loading = LoadingProvider();
  final alertProvider = AlertProvider();


  @override
  Widget build(BuildContext context) {


    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            color: Color.fromRGBO(79, 189, 172, 1),
          ),
          _createBackgroundCircle(size),
          _createContent(size),
        ],
      )
    );
  }

  Widget _createBackgroundCircle(Size size) {
    return Positioned(
      width: size.height,
      top: - size.height * 0.3,
      left: - size.height *0.25,
      child: Container(
        height: size.height +100,
        decoration: BoxDecoration(
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.black26,
              blurRadius: 3.0,
              offset: Offset(0.0,5.0),
              spreadRadius: 3
            )
          ],
          color: Colors.white,
          borderRadius: BorderRadius.circular(1000)
        ),
      ),
    );
  }

  Widget _createContent(Size size) {
    
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SizedBox(height: 65),
          Text('REGISTER', style: TextStyle(fontSize: 25, fontWeight: FontWeight.w900) ),
          SizedBox(height: 50),
          _createFormBox(size),
          _createActionButtons(size),
        ],
      ),
    );
  }

  Widget _createFormBox(Size size){
    return Stack(
      overflow: Overflow.visible,
      children: [
        _createLoginForm(size),
        _createIconAccount(size),
        _createRegistrerButton(size)
      ],
    );
  }

  Widget _createLoginForm(Size size) {
    return Container(
      padding: EdgeInsets.all(30),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(7),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black26,
            blurRadius: 20.0,
            spreadRadius: 10
          )
        ]
      ),
      width: size.width * 0.7,
      child: Form(
        key: _keyFrom,
        child: Column(
          children: <Widget>[
            SizedBox(height: 30,),
            _createUserNameField(),
            SizedBox(height: 30,),
            _createNameField(),
            SizedBox(height: 30,),
            _createPasswordField(),
            SizedBox(height: 30,),
            _createPasswordConfirmField(),
            SizedBox(height: 30,),
          ],
        ),
      )
    );
  }

  Widget _createIconAccount(Size size) {
    return Positioned(
      top: -35,
      left: (size.width * 0.7) / 2 - 35,
      child: Container(
        height: 70,
        width: 70,
        decoration: BoxDecoration(
          color: Color.fromRGBO(79, 189, 172, 1),
          borderRadius: BorderRadius.circular(70),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              spreadRadius: 5
            )
          ]
        ),
        child: SizedBox(
          width: double.infinity,
          child: Icon(Icons.person, size: 50, color: Colors.white)
        ),
      )
    );
  }

  Widget _createRegistrerButton(Size size) {
    return Positioned(
      bottom: -25,
      left: (size.width * 0.7) / 2 - 100,
      child: RaisedButton(
        elevation: 15,
        color: Color.fromRGBO(79, 189, 172, 1),
        textColor: Colors.white,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 40, vertical: 15),
          child: Text('REGISTER', style: TextStyle(fontSize: 20),)
        ),
        onPressed: _onSubmit,
      ),
    );
  }

  Widget _createActionButtons(Size size){
    return Container(
      margin: EdgeInsets.only(top: 70),
      width: size.width * 0.7,
      child: Column(
        children: <Widget>[
          FlatButton(
            child: Text('I already got an account', style: TextStyle(color: Colors.white),),
            onPressed: () => Navigator.pop(context),
          ),
        ],
      ),
    );
  }

  void _onSubmit() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    if (!_keyFrom.currentState.validate()) return;

    _keyFrom.currentState.save();
    loading.start(context);
    final createAccount = await authProvider.createAccount(_user, password);
    loading.stop(context);
    if (createAccount.ok){
      Navigator.pop(context);
    }else {
      alertProvider.showErrorMessage(createAccount.msn, context);
    }

  }

  Widget _createUserNameField() {
    return Container(
      child: TextFormField(
        initialValue: _user.email,
        keyboardType: TextInputType.emailAddress,
        textAlign: TextAlign.center,
        decoration: _inputDecoration('Email'),
        validator: (value){
          if (!emailValidator(value)) return 'Email invalid';
          
          return null;
        },
        onSaved: (value) => _user.email = value,

      )
    );
  }

  Widget _createNameField() {
    return Container(
      child: TextFormField(
        initialValue: _user.displayName,
        keyboardType: TextInputType.emailAddress,
        textAlign: TextAlign.center,
        decoration: _inputDecoration('Name'),
        validator: (value){
          if (value.length < 3) return 'The name is too short';
          return null;
        },  
        onSaved: (value) => _user.displayName = value,

      )
    );
  }

  Widget _createPasswordField(){
    return Container(
      child: TextFormField(
        key:_passKey,
        initialValue: password,
        textAlign: TextAlign.center,
        obscureText: true,
        decoration: _inputDecoration('Password'),
        validator: (value){
          if (value.length < 6) return 'Password must be 6 caracters';
          if (value != _passConfirmKey.currentState.value) return 'Password don\'t match';
          return null;
        },
        onSaved: (value) => password = value,
      )
    );
  }

  Widget _createPasswordConfirmField(){
    return Container(
      child: TextFormField(
        key: _passConfirmKey,
        textAlign: TextAlign.center,
        obscureText: true,
        decoration: _inputDecoration('Confirm Password'),
        validator: (value){
          if (value.length < 6) return 'Password must be 6 caracters';
          if (value != _passKey.currentState.value) return 'Password don\'t match';
          return null;
        },
      )
    );
  }

  _inputDecoration(String label) {
    return InputDecoration(
      labelText: label,
      filled: true,
      fillColor: Color.fromRGBO(79, 189, 172, 0.1),
      hasFloatingPlaceholder: false,
      enabledBorder: OutlineInputBorder(
        borderSide: new BorderSide(color: Colors.transparent),
        borderRadius: const BorderRadius.all(
          Radius.circular(10.0),
        )),
      border: OutlineInputBorder(
        borderSide: new BorderSide(color: Colors.transparent),
        borderRadius: const BorderRadius.all(
          Radius.circular(10.0),
        ),
        
      ),
      
    );
  }
}