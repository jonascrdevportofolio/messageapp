import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:messages/src/pages/account/login_page.dart';
import 'package:messages/src/pages/messages/chats_page.dart';
import 'package:messages/src/providers/auth_provider.dart';
import 'package:messages/src/routes/main_routes.dart';
import 'package:messages/src/shared_preference/user_preference.dart';
 
void main()async {
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  final prefs = UserPreference();
  await prefs.initPrefs();
  runApp(MessageApp());

}
 
class MessageApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //Check if the user is logged or not
    return StreamBuilder(
      stream: authProvider.user,
      builder: (BuildContext context, AsyncSnapshot<FirebaseUser> snapshot) {
        if (snapshot.hasData){
          //User is logged => go to home page
          return mainApp(context, ChatsPage(idCurrentUser: snapshot.data.uid));
        }else {
          //User is not logged => go to login page
          return mainApp(context, LoginPage());
        }
      },
    );
  }

  Widget mainApp(BuildContext context, Widget initialRoute){
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      debugShowMaterialGrid: false,
      title: 'Message App',
      home: initialRoute,
      routes: getMainRoutes(),
      theme: Theme.of(context).copyWith(
        accentColor: Colors.transparent,
        primaryColor: Color.fromRGBO(79, 189, 172, 1),
        pageTransitionsTheme: PageTransitionsTheme(
          builders: {
            TargetPlatform.android: CupertinoPageTransitionsBuilder(),
          }
        ),

      ),
    );
  }
}